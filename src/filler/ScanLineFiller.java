package filler;

import model.Edge;
import model.Point;
import model.Polygon;
import raster.LineRasterizer;
import raster.PolygonRasterizer;

import java.util.ArrayList;
import java.util.List;

public class ScanLineFiller implements Filler {
    private final LineRasterizer lineRasterizer;
    private final PolygonRasterizer polygonRasterizer;
    private final Polygon polygon;

    public ScanLineFiller(LineRasterizer lineRasterizer, PolygonRasterizer polygonRasterizer, Polygon polygon) {
        this.lineRasterizer = lineRasterizer;
        this.polygonRasterizer = polygonRasterizer;
        this.polygon = polygon;
    }

    @Override
    public void fill() {
        scanLine();
    }

    private void scanLine() {
        // Vytvořit hrany
        List<Edge> edges = new ArrayList<>();

        // Projít pointy polygonu a vytvořit z nich hrany
        for (int i = 0; i < polygon.getCount(); i++) {
            Point p1 = polygon.getPoint(i);
            int index = (i + 1) % polygon.getCount();
            Point p2 = polygon.getPoint(index);
            Edge edge = new Edge(p1.getX(), p1.getY(), p2.getX(), p2.getY());

            // Pokud je hrana horizontální, nepřidáváme
            if (edge.isHorizontal())
                continue;

            // Změním orientaci
            edge.orientate();
            // Přidám hhranu do seznamu
            edges.add(edge);
        }

        // Najdu yMin a yMax
        int yMin = polygon.getPoint(0).getY();
        int yMax = yMin;
        for (Point p : polygon.getPoints()) {
            // TODO: Najít ymin a ymax
        }

        // Pro všechna y od yMin po yMax
        for (int y = yMin; y <= yMax; y++) {
            // Seznam průsečíků: List<Integer>

            // Pro všechny hrany
            // {
                // Existuje průsečík?
                // Ano - spočítám průsečík
                // Uložíme do seznamu průsečíků
            // }

            // setřídění průsečíků podle x, např. BubleSort

            // spojit průsečíky linou, lichý se sudým
        }

        // vykreslit polygon - hranici



    }
}
