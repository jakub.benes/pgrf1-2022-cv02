package filler;

import raster.Raster;

public class SeedFiller implements Filler {

    private final int x, y;
    private Raster raster;
    private final int fillColor, backgroundColor;


    public SeedFiller(int x, int y, int fillColor, int backgroundColor, Raster raster) {
        this.x = x;
        this.y = y;
        this.fillColor = fillColor;
        this.backgroundColor = backgroundColor;
        this.raster = raster;
    }

    @Override
    public void fill() {
        seedFill(x, y);
    }

    private void seedFill(int x, int y) {
        // načtu barvu z pixelu
        int pixelColor = raster.getPixel(x, y);

        // podmínka: pokud barva pixelu != barvě pozadí -> skončím
        if (pixelColor != backgroundColor)
            return;

        // obarvím pixel
        raster.setPixel(x, y, fillColor);

        // rekurzivně zavolám seedFill pro 4 sousedy
        seedFill(x, y + 1);
        seedFill(x, y - 1);
        seedFill(x - 1, y);
        seedFill(x + 1, y);
    }
}
