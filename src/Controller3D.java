import filler.Filler;
import filler.SeedFiller;
import model.Point;
import model.Polygon;
import raster.*;
import solids.Cube;
import solids.Solid;
import transforms.Mat4;
import transforms.Mat4Scale;
import transforms.Mat4Transl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;


public class Controller3D {
    private final JFrame frame;
    private final JPanel panel;
    private final Raster raster;
    private final LineRasterizer lineRasterizer;
    private final WireRenderer wireRenderer;
    private Solid cube;


    public Controller3D(int width, int height)
    {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("PGRF1");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        raster = new RasterBufferedImage(width, height);
        lineRasterizer = new LineRasterizerGraphics(raster);
        wireRenderer = new WireRenderer(lineRasterizer);

        panel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(((RasterBufferedImage)raster).getImg(), 0,0, null);
            }
        };
        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        panel.requestFocus();
        panel.requestFocusInWindow();
    }

    public void start() {
        raster.clear();

        Solid cube1 = new Cube();
        Solid cube2 = new Cube();

        Mat4 scale = new Mat4Scale(0.5);
        Mat4 transl = new Mat4Transl(0.3, 0.3, 0);
        cube2.setModel(scale.mul(transl));
        cube1.setModel(scale);

        List<Solid> scene = new ArrayList<>();
        scene.add(cube1);
        scene.add(cube2);

        wireRenderer.renderScene(scene);

        panel.repaint();
    }
}
